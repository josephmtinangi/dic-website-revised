// script.js

// create the module and name it dicApp
var dicApp = angular.module('dicApp', ['ngRoute']);

// configure routes
dicApp.config(function($routeProvider) {
  $routeProvider

  // route for the home page
  .when('/', {
    templateUrl : 'pages/home.html',
    controller : 'mainController'
  })

  // route for the donate page
  .when('/donate', {
    templateUrl : 'pages/donate.html',
    controller : 'donateController'
  })  

  // route for the about page
  .when('/about', {
    templateUrl : 'pages/about.html',
    controller : 'aboutController'
  })

  // route for the services page
  .when('/what-we-do', {
    templateUrl : 'pages/services.html',
    controller : 'servicesController'
  })  

  // route for the gallery page
  .when('/gallery', {
    templateUrl : 'pages/gallery.html',
    controller : 'galleryController'
  })   

  // route for the contact page
  .when('/contact', {
    templateUrl : 'pages/contact.html',
    controller : 'contactController'
  });
});

// create the controller and inject Angular's $scope
dicApp.controller('mainController', function($scope) {

  // create a message to display in the view
});

dicApp.controller('donateController', function($scope) {
  $scope.message = 'Sharing is kind!.';
});

dicApp.controller('aboutController', function($scope) {
  $scope.message = 'Dalai Islamic Center';
});

dicApp.controller('servicesController', function($scope) {
  $scope.message = 'What we do.'
});

dicApp.controller('galleryController', function($scope) {
  $scope.message = 'Image Gallery.'
});

dicApp.controller('contactController', function($scope) {
  $scope.message = 'We will respond as soon as we can.';
});